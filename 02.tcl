#!/usr/bin/wish

set strategyList [list]

proc listFromFile {filename} {
    set f [open $filename r]
    set data [split [string trim [read $f]]]
    close $f
    return $data
}


proc calculateMySuperiorStrategy {nr} {
    # in dumb way: odd values of list = 1st player
    #              even values of list = 2nd player
    # A = rock      Y = paper
    # B = paper     X = rock
    # C = scissors  Z = scissors
    # points:  choosing  Y = +2
    #                    X = +1
    #                    Z = +3
    #                    win = +6    draw = +3   loose = 0

    global strategyList
    set playerOne [list]
    set playerTwo [list]
    set points 0
    set odd 0
    set even 1
    # splitting the list into two, for each player
    foreach {odd even} $strategyList {
	lappend playerOne $odd
	lappend playerTwo $even
    }
    for {set i 0} {$i < [llength $playerOne]} {incr i} {
	set P1 [lindex $playerOne $i]
	set P2 [lindex $playerTwo $i]
	if {$P1 == "A" && $P2 == "Y"} {
	    set points [expr ($points + 2 + 6)]
	}
	if {$P1 == "A" && $P2 == "X"} {
	    set points [expr ($points + 1 + 3)]
	}
	if {$P1 == "A" && $P2 == "Z"} {
	    set points [expr ($points + 3 + 0)]
	}
	if {$P1 == "B" && $P2 == "Y"} {
	    set points [expr ($points + 2 + 3)]
	}
	if {$P1 == "B" && $P2 == "X"} {
	    set points [expr ($points + 1 + 0)]
	}
	if {$P1 == "B" && $P2 == "Z"} {
	    set points [expr ($points + 3 + 6)]
	}
	if {$P1 == "C" && $P2 == "Y"} {
	    set points [expr ($points + 2 + 0)]
	}
	if {$P1 == "C" && $P2 == "X"} {
	    set points [expr ($points + 1 + 6)]
	}
	if {$P1 == "C" && $P2 == "Z"} {
	    set points [expr ($points + 3 + 3)]
	}
    }
    return $points
}

proc calculateMyEvenBetterStrategy {nr} {
    # in dumb way: odd values of list = 1st player
    #              even values of list = 2nd player
    # A = rock      Y = draw
    # B = paper     X = loose
    # C = scissors  Z = win
    # points:  choosing  paper = +2
    #                    rock  = +1
    #                    sciss.= +3
    #                    win = +6    draw = +3   loose = 0

    global strategyList
    set playerOne [list]
    set playerTwo [list]
    set points 0
    set odd 0
    set even 1
    # splitting the list into two, for each player
    foreach {odd even} $strategyList {
	lappend playerOne $odd
	lappend playerTwo $even
    }
    for {set i 0} {$i < [llength $playerOne]} {incr i} {
	set P1 [lindex $playerOne $i]
	set P2 [lindex $playerTwo $i]
	if {$P1 == "A" && $P2 == "Y"} {
	    set points [expr ($points + 1 + 3)]
	}
	if {$P1 == "A" && $P2 == "X"} {
	    set points [expr ($points + 3 + 0)]
	}
	if {$P1 == "A" && $P2 == "Z"} {
	    set points [expr ($points + 2 + 6)]
	}
	if {$P1 == "B" && $P2 == "Y"} {
	    set points [expr ($points + 2 + 3)]
	}
	if {$P1 == "B" && $P2 == "X"} {
	    set points [expr ($points + 1 + 0)]
	}
	if {$P1 == "B" && $P2 == "Z"} {
	    set points [expr ($points + 3 + 6)]
	}
	if {$P1 == "C" && $P2 == "Y"} {
	    set points [expr ($points + 3 + 3)]
	}
	if {$P1 == "C" && $P2 == "X"} {
	    set points [expr ($points + 2 + 0)]
	}
	if {$P1 == "C" && $P2 == "Z"} {
	    set points [expr ($points + 1 + 6)]
	}
    }
    return $points
}


### main ###

set strategyList [listFromFile filelist.txt]
puts "Task 1 - Total score based on strategy list: [calculateMySuperiorStrategy [llength $strategyList]]"
puts "Task 2 - Total score based on EVEN BETTER strategy list: [calculateMyEvenBetterStrategy [llength $strategyList]]"

## 02.tcl ends here ##
